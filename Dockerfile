FROM debian:bullseye

ARG APP_UID=2000

ARG APP_GID=2000

MAINTAINER TANGO Controls team <tango@esrf.fr>

ENV DEBIAN_FRONTEND=noninteractive
ENV MAKEFLAGS="-j 12"

RUN apt-get update &&                                                     \
  apt-get install --assume-yes --no-install-recommends ca-certificates && \
  apt-get clean

RUN echo "deb https://mirror.byte-physics.de/debian bullseye main contrib non-free" > /etc/apt/sources.list &&                                  \
  echo "deb https://mirror.byte-physics.de/debian-security/debian-security bullseye-security main contrib non-free" >> /etc/apt/sources.list && \
  echo "deb-src https://mirror.byte-physics.de/debian bullseye main contrib non-free" >> /etc/apt/sources.list

RUN apt-get update && apt-get install -y \
  apt-utils                              \
  build-essential                        \
  cmake                                  \
  curl                                   \
  gcovr                                  \
  git                                    \
  libcos4-dev                            \
  libomniorb4-dev                        \
  libomnithread4-dev                     \
  libzmq3-dev                            \
  lsb-release                            \
  omniidl                                \
  libjpeg-dev                            \
  libboost-python-dev                    \
  python3                                \
  python-is-python3                      \
  python3-pip                            \
  python3-numpy

WORKDIR /root

RUN git clone  -b main --depth 1 https://gitlab.com/tango-controls/tango-idl.git
RUN git clone -b v4.7.1 --depth 1 https://github.com/zeromq/cppzmq.git
RUN git clone -b bugfix/allow-empty-hostnames-in-omniORB-variables-again --depth 1 https://gitlab.com/tango-controls/cppTango.git
RUN git clone -b develop --depth 1 https://gitlab.com/tango-controls/pytango.git 

WORKDIR /root/tango-idl

RUN cmake -B build . &&   \
    make -C build install

WORKDIR /root/cppzmq

RUN cmake -B build -DCPPZMQ_BUILD_TESTS=OFF . && \
    make -C build install

WORKDIR /root/cppTango
 
RUN cmake -B build . -DTANGO_USE_JPEG=OFF -DBUILD_TESTING=OFF && \
    make -C build install

WORKDIR /root/pytango

RUN python -m pip install .

WORKDIR /root

RUN echo "/usr/local/lib" > /etc/ld.so.conf.d/local.conf && ldconfig

RUN python -c "import tango; print(tango.utils.info())"
