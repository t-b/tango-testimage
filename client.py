import tango
import os

dp=tango.DeviceProxy(f"tango://{os.environ['TANGO_HOST']}:10000/dserver/test_device/MyDevice")

dp.set_logging_level(5);

print(dp.ZmqEventSubscriptionChange(["my/own/device","status","0","PERIODIC"]))

dp.poll_attribute("Status", 3000)

dp.subscribe_event("Status", tango.EventType.PERIODIC_EVENT, tango.utils.EventCallback())
