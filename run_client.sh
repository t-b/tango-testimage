docker run --rm -it                             \
  -e TANGO_HOST=${SPECIAL_HOSTNAME}:10000       \
  -v $PWD:/test                                 \
  tango-allimage                                \
  python /test/client.py
