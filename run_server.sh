set -x 

docker run --rm -it                                     \
  -e TANGO_HOST=${SPECIAL_HOSTNAME}:10000               \
  -e TANGO_ZMQ_EVENT_PORT=5556                          \
  -e TANGO_ZMQ_HEARTBEAT_PORT=5557                      \
  -p 5555:5555 -p 5556:5556 -p 5557:5557                \
  -v $PWD:/test                                         \
  tango-allimage                                        \
  python /test/test_device.py MyDevice -v5              \
  -ORBendPointPublish giop:tcp:${SPECIAL_HOSTNAME}:5555 \
  -ORBendPoint giop:tcp::5555
