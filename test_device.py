from tango.server import Device, run
from tango import Database, DbDevInfo

class MyDevice(Device):
    pass

def main(args=None, **kwargs):
    db = Database()
    dev_info = DbDevInfo()
    dev_info.name = 'my/own/device'
    dev_info._class = 'MyDevice'
    dev_info.server = 'test_device/MyDevice'
    db.add_device(dev_info)
    return run((MyDevice,), args=args, **kwargs)

if __name__ == '__main__':
    main()
